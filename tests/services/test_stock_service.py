import pymongo
import pytest

from server.services import stock_service


class DummyAppContext:
    pass


@pytest.fixture
def dummy_app():
    dummy_app = DummyAppContext()
    stock_service.current_app = dummy_app
    return dummy_app


def test_init_db(dummy_app, mocker):
    mocker.patch('pymongo.MongoClient')

    stock_service.init_db()

    pymongo.MongoClient.assert_called_once()


def test_get_all(dummy_app, mocker):
    items = [{'_id': '123abc', 'ticker': 'GOOG', 'amount': 100},
             {'_id': '345f2', 'ticker': 'AAPL', 'amount': 23}]

    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.find.return_value = items

    assert stock_service.get_all() == items


def test_get_stock(dummy_app, mocker):
    item = {'_id': '123abc', 'ticker': 'GOOG', 'amount': 100}
    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.find_one.return_value = item

    assert stock_service.get_stock('AMZN') == item


def test_create_stock(dummy_app, mocker):
    mocker.patch('server.services.stock_service.get_stock',
                 return_value=None)

    insert_id = 'abcd2345'
    dummy_app._db_collection = mocker.MagicMock()
    dummy_app._db_collection.insert_one.return_value = \
        pymongo.results.InsertOneResult(insert_id, True)

    assert stock_service.create_stock({'ticker': 'AAPL'}) == insert_id


def test_create_stock_already_exists(dummy_app, mocker):
    mocker.patch('server.services.stock_service.get_stock',
                 return_value='AAPL')

    with pytest.raises(Exception):
        stock_service.create_stock({'ticker': 'AAPL'})
