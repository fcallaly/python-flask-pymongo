import logging
import os

import pymongo

from flask import current_app

LOG = logging.getLogger(__name__)


def init_db():
    mongo_host = os.getenv('MONGO_HOST', 'localhost')
    mongo_port = os.getenv('MONGO_PORT', 27017)
    stock_db = os.getenv('STOCK_DB', 'stockdb')
    stock_collection = os.getenv('STOCK_COLLECTION', 'stocks')

    if(hasattr(current_app, '_mongodb') and
       hasattr(current_app, '_db_collection')):
        return

    LOG.info('Initialising MongoDB resource: ' +
             str(mongo_host) + ':' + str(mongo_port))

    current_app._mongodb = pymongo.MongoClient(host=mongo_host,
                                               port=mongo_port)
    current_app._db_collection = \
        current_app._mongodb[stock_db][stock_collection]


def get_all(limit=10, esk=None):
    LOG.debug('get all')

    if esk:
        response = list(current_app._db_collection.find())
    else:
        response = list(current_app._db_collection.find())

    LOG.debug('Received ' + str(response) + ' from DB')

    return response


def get_stock(ticker):
    LOG.debug('get: ' + str(ticker))

    response = current_app._db_collection.find_one({'ticker': ticker})

    LOG.debug('Received ' + str(response) + ' from DB')

    return response


def create_stock(stock_resource):
    LOG.debug('create: ' + str(stock_resource))

    if get_stock(stock_resource['ticker']):
        LOG.debug('Stock already exists')
        raise Exception('Stock already exists ' +
                        str(stock_resource['ticker']))

    response = current_app._db_collection.insert_one(stock_resource)

    LOG.debug('Received ' + str(response) + ' from DB')

    if response is None:
        LOG.debug('Error creating new Stock')
        raise Exception('Error creating new Stock: ' + str(stock_resource))

    return response.inserted_id
