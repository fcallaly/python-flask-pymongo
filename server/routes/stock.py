import json
import logging

from flask import jsonify, request, Response
from flask_restx import abort, Namespace, Resource, fields
from server import app, api

from server.services import stock_service

LOG = logging.getLogger(__name__)

stock_namespace = Namespace('stock',
                            description='Interface for Stock Resource')

api.add_namespace(stock_namespace)

stock_fields = api.model('Stock',
                         {
                            'ticker': fields.String,
                            'amount': fields.Integer
                         })


@stock_namespace.route("")
class Stock(Resource):

    @api.param('ticker', description='stock ticker', type='string')
    def get(self):
        if not request.args.get("ticker"):
            response = stock_service.get_all()
        else:
            response = stock_service.get_stock(request.args.get("ticker"))

        if response is None:
            abort(404, message='Stock not found for ticker: ' +
                               str(request.args.get("ticker")))
        return Response(json.dumps(response, default=str),
                        mimetype="application/json")

    @api.expect(stock_fields)
    def post(self):
        LOG.debug('Create new Stock: ' + str(api.payload))
        try:
            stock_service.create_stock(api.payload)
        except Exception as e:
            return {'success': False, 'message': str(e)}, 400

        return {'success': True}, 201
